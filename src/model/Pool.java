package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fedor on 7.3.17.
 */
public class Pool {

    private List<Junction> junctions;

    public Pool() {
        junctions = new ArrayList<>();
    }

    public List<Junction> getJunctions() {
        return junctions;
    }

    public void add(Junction junction) {
        this.junctions.add(junction);
    }
}
