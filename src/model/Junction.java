package model;

/**
 * Created by fedor on 7.3.17.
 */
public class Junction {

    private String lat;
    private String lon;
    private String firstRoad;
    private String secondRoad;
    private String query;

    public Junction() {

    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getFirstRoad() {
        return firstRoad;
    }

    public void setFirstRoad(String firstRoad) {
        this.firstRoad = firstRoad;
    }

    public String getSecondRoad() {
        return secondRoad;
    }

    public void setSecondRoad(String secondRoad) {
        this.secondRoad = secondRoad;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return "Junction{" +
                "lat='" + lat + '\'' +
                ", lon='" + lon + '\'' +
                ", firstRoad='" + firstRoad + '\'' +
                ", secondRoad='" + secondRoad + '\'' +
                ", query='" + query + '\'' +
                '}';
    }
}
