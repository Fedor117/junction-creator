package view;

import controller.PanelController;

import javax.swing.*;

/**
 * Created by fedor on 7.3.17.
 */
public class FooterPanel extends JPanel {

    private JButton createButton;
    private JButton saveButton;

    public FooterPanel() {
        init();
    }

    private void init() {
        createButton = new JButton("Create");
        createButton.addActionListener(e -> PanelController.createJunction());

        saveButton = new JButton("Save all");
        saveButton.addActionListener(e -> PanelController.savePool());

        this.add(createButton);
        this.add(saveButton);
    }
}
