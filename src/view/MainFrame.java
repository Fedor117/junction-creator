package view;

import javax.swing.*;

/**
 * Created by fedor on 7.3.17.
 */
public class MainFrame extends JFrame {

    public MainFrame() {
        super("JUNK-tions!");

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(300, 300);

        this.add(new MainPanel());

        this.pack();
        this.setVisible(true);
    }

}
