package view;

import javax.swing.*;

/**
 * Created by fedor on 7.3.17.
 */
public class MemoPanel extends JPanel {

    private JTextArea textArea;

    public MemoPanel() {
        init();
    }

    private void init() {
        textArea = new JTextArea(30, 50);
        textArea.setEditable(false);

        JScrollPane pane = new JScrollPane(textArea);

        this.add(pane);
    }

    public void log(String msg) {
        textArea.append(msg + "\n");
    }
}
