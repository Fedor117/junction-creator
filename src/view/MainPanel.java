package view;

import controller.PanelController;

import javax.swing.*;
import java.awt.*;

/**
 * Created by fedor on 7.3.17.
 */
public class MainPanel extends JPanel {

    public MainPanel() {
        init();
    }

    private void init() {
        EntryPanel entry = new EntryPanel();
        MemoPanel memo = new MemoPanel();
        FooterPanel footer = new FooterPanel();

        PanelController.setEntry(entry);
        PanelController.setMemo(memo);
        PanelController.setFooter(footer);

        this.setLayout(new BorderLayout());

        this.add(entry, BorderLayout.WEST);
        this.add(memo, BorderLayout.CENTER);
        this.add(footer, BorderLayout.SOUTH);
    }

}
