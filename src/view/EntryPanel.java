package view;

import javax.swing.*;

/**
 * Created by fedor on 7.3.17.
 */
public class EntryPanel extends JPanel {

    private static final int SIZE = 10;

    private JTextField firstRoad;
    private JTextField secondRoad;
    private JTextField lat;
    private JTextField lon;

    public EntryPanel() {
        init();
    }

    private void init() {
        firstRoad = new JTextField(SIZE);
        secondRoad = new JTextField(SIZE);
        lat = new JTextField(SIZE);
        lon = new JTextField(SIZE);

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        this.add(new JLabel("First road:"));
        this.add(firstRoad);

        this.add(new JLabel("Second road:"));
        this.add(secondRoad);

        this.add(new JLabel("Lat:"));
        this.add(lat);

        this.add(new JLabel("Lon:"));
        this.add(lon);
    }

    public JTextField getFirstRoad() {
        return firstRoad;
    }

    public JTextField getSecondRoad() {
        return secondRoad;
    }

    public JTextField getLat() {
        return lat;
    }

    public JTextField getLon() {
        return lon;
    }

}
