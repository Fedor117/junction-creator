package controller;

import model.Junction;
import model.Pool;
import view.EntryPanel;
import view.FooterPanel;
import view.MemoPanel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by fedor on 7.3.17.
 */
public class PanelController {

    private static final String SAVE_PATH = "generated/";
    private static final String FILE_TYPE = ".scs";

    private static EntryPanel entry;
    private static FooterPanel footer;
    private static MemoPanel memo;

    private static Pool pool = new Pool();

    public static void createJunction() {
        Junction junction = new Junction();

        junction.setFirstRoad(entry.getFirstRoad().getText());
        junction.setSecondRoad(entry.getSecondRoad().getText());
        junction.setLat(entry.getLat().getText());
        junction.setLon(entry.getLon().getText());
        junction.setQuery(createQuery());

        pool.add(junction);

        memo.log("Added to pool: " + junction.toString());
    }

    public static String createQuery() {
        return "[bbox:{{bbox}}]; \n" +
                "way[\"highway\"=\"primary\"][\"ref\"=\"" + entry.getFirstRoad().getText() + "\"];node(w)->.n1; \n" +
                "way[\"highway\"=\"trunk\"][\"ref\"=\"\"" + entry.getSecondRoad().getText() + "\"];node(w)->.n2; \n" +
                "node.n1.n2; \n" +
                "out meta;";
    }

    public static void savePool() {
        memo.log("Starting save procedure");
        int count = 0;

        createDir();

        for (Junction junction : pool.getJunctions()) {
            memo.log("Saving " + junction.getFirstRoad() + "-" + junction.getSecondRoad());
            createArticle(junction);
            count++;
        }

        memo.log("Saved " + count + "articles");
    }

    private static void createArticle(Junction junction) {
        String article_type = "sc_node_not_relation";
        String name = "concept_junction_" + junction.getFirstRoad() + "-" + junction.getSecondRoad();
        String type = "concept_junction";
        String identification = "nrel_main_idtf";
        String nrelLat = "nrel_latitude";
        String nrelLon = "nrel_longitude";
        String osm = "nrel_osm_query";
        String langRu = "(* <- lang_ru;; *)";
        String langEn = "(* <- lang_en;; *)";
        String tab = "\t";
        String separator = ";;";
        String line = "\n";
        String next = separator + line + line;

        StringBuilder builder = new StringBuilder(512);

        builder.append(article_type + " -> " + name + next);
        builder.append(name + " <- " + type + next);

        builder.append(name + " => " + identification + ":" + line);
        builder.append(tab + "[перекресток " + junction.getFirstRoad() + "-" + junction.getSecondRoad() + "]" + line);
        builder.append(tab + langRu + ";" + line);
        builder.append(tab + "[junction " + junction.getFirstRoad() + "-" + junction.getSecondRoad() + "]" + line);
        builder.append(tab + langEn + next);

        builder.append(name + " => " + nrelLat + ":" + line);
        builder.append(tab + "[" + junction.getLat() + "]" + next);

        builder.append(name + " => " + nrelLon + ":" + line);
        builder.append(tab + "[" + junction.getLon() + "]" + next);

        builder.append(name + " => " + osm + ":" + line);
        builder.append(tab + "[" + junction.getQuery() + "]" + next);

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(
                    new File(SAVE_PATH + name + FILE_TYPE)));
            writer.write(builder.toString());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createDir() {
        File dir = new File(SAVE_PATH);
        boolean created = false;

        if (!dir.exists()) {
            try {
                created = dir.mkdir();
            } catch (SecurityException se) {
                se.printStackTrace();
            }
        }

        if (created) {
            memo.log("Created directory 'generated'");
        }
    }

    public static EntryPanel getEntry() {
        return entry;
    }

    public static void setEntry(EntryPanel entry) {
        PanelController.entry = entry;
    }

    public static FooterPanel getFooter() {
        return footer;
    }

    public static void setFooter(FooterPanel footer) {
        PanelController.footer = footer;
    }

    public static MemoPanel getMemo() {
        return memo;
    }

    public static void setMemo(MemoPanel memo) {
        PanelController.memo = memo;
    }
}
